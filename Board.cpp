#include <iostream>
#include <utility>
#include <map>
#ifndef _WIN32
#include "Terminal.h"
#endif // !_WIN32
#include "Board.h"
#include "CreatePiece.h"

#include <algorithm>
#include <string>
using std::string;
using std::cout;
using std::endl;
/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board( void ){}

Board::~Board( void ) {
  //cout << "deconstructing the board" << endl;
  for(std::map< std::pair< char , char > , Piece* >::const_iterator it = _occ.begin();
      it != _occ.cend();
      ++it)
    {
      //cout << "deleting piece..." << it->second->to_ascii() << endl;
      delete it->second;
    } 
}

void Board::delete_at(std::pair<char, char> position) {
  //delete _occ[position];
  _occ.erase(position);
}
const Piece* Board::operator()( std::pair< char , char > position ) const {
  //use map::find() to see if a piece exists at the location
  std::map< std::pair< char , char > , Piece* >::const_iterator it;
  //
  it = _occ.find(position);
  if (it == _occ.cend()) {
    return NULL;
  }
  
  const Piece* copy = _occ.at(position);
  return copy;
}

bool Board::add_piece( std::pair< char , char > position , char piece_designator )
{
  const string valid_pieces = "MmKQRBPNkqrpbn";
  bool char_found = false;
  
  //compare the piece_designator with the set of valid pieces
  for(string::const_iterator it = valid_pieces.begin();
      it != valid_pieces.cend();
      ++it)
    {
      if (*it == piece_designator) {
	char_found = true;
      }
    }
  if (!char_found) {
    return false;
  }

  //check if a piece already exists at this position
  if ((*this)(position) != NULL) {
    return false;
    }
  
  //check if the position is within the board 
  if (position.first < 'A' || position.first > 'H'
      || position.second < '1' || position.second > '8') {
    return false;
  }
  
  //for every invalid case, make sure to return false before this code runs
  _occ[ position ] = create_piece( piece_designator );
  return true;
}

//there must be 1 white king and 1 black king
bool Board::has_valid_kings( void ) const
{
  int white_kings = 0;
  int black_kings = 0;

  //iterate through the map and count the number of kings
  for(std::map< std::pair< char , char > , Piece* >::const_iterator it = _occ.begin();
      it != _occ.cend();
      ++it)
    {
      if (it->second->to_ascii() == 'K') {
	white_kings++;
      }
      if (it->second->to_ascii() == 'k') {
	black_kings++;
      }
    }

  return (white_kings == 1 && black_kings == 1);
}

void Board::display( void ) const {
    bool black = false;
    cout << endl;
  Terminal::color_bg(Terminal::MAGENTA);
  Terminal::color_fg(true, Terminal::WHITE);
  cout<<"    A  B  C  D  E  F  G  H    ";
  for (int i = '8'; i >= '1'; i--) {
    Terminal::set_default();
    cout<<endl;
    Terminal::color_bg(Terminal::MAGENTA);
    Terminal::color_fg(true, Terminal::WHITE);
    cout<<" "<< (char) i<< " ";
    for (int j = 'A'; j<='H'; j++) {
      const Piece* piece = (*this)(std::pair<char, char>(j, i));
      if (black) {
        Terminal::color_bg(Terminal::CYAN);
      } else {
        Terminal::color_bg(Terminal::YELLOW);
      }
      black = !black;
      if (piece ) {
        if ((*piece).is_white()) {
          Terminal::color_fg(true, Terminal::WHITE);
          cout<<" "<<piece->to_ascii()<< " ";
        } else {
          Terminal::color_fg(false, Terminal::BLACK);
          cout<<" "<<(char)(piece->to_ascii()-('a'- 'A'))<< " ";

        }
      } else {
        //Terminal::color_fg(true, Terminal::RED);
        cout << "   ";
      }
    }
    Terminal::color_bg(Terminal::MAGENTA);
    Terminal::color_fg(true, Terminal::WHITE);
    cout<< " "<< (char) i << " ";
    black= !black;
  }
  Terminal::set_default();
  cout <<endl;
  Terminal::color_bg(Terminal::MAGENTA);
  Terminal::color_fg(true, Terminal::WHITE);
  cout <<"    A  B  C  D  E  F  G  H    ";
  Terminal::set_default();
  cout<<endl;

}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Board& board )
{
  for( char r='8' ; r>='1' ; r-- )
    {
      for( char c='A' ; c<='H' ; c++ )
	{
	  const Piece* piece = board( std::pair< char , char >( c , r ) );
	  if( piece ) os << piece->to_ascii();
	  else        os << '-';
	}
      os << std::endl;
    }
  return os;
}
