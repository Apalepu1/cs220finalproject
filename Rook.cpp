//Rook.cpp

#include "Rook.h"
#include "Piece.h"

#include <tuple>
using std::pair;

//legal example, b1 to b4
//illegal example, c1 to b3
bool Rook::legal_move_shape( pair< char , char > start , pair< char , char > end ) const {

  //either the column or the row must be the same
  if (start.first == end.first) {
    return true;
  }

  if (start.second == end.second) {
    return true;
  }
  return false;    
}
