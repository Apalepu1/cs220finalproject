//King.cpp
#include "King.h"
#include "Piece.h"
#include <algorithm>
#include <cmath>

using std::max;
using std::abs;
using std::pair;

bool King::legal_move_shape( pair<char, char> start, pair<char, char> end) const {

  int firstDist = abs(start.first - end.first);
  int secondDist = abs(start.second - end.second);
  //Should be max of 1 unit away
  if (max(firstDist, secondDist) <=1) {
    return true;
  } else {
    return false;
  }
}
