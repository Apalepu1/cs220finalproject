//Queen.cpp

#include "Queen.h"
#include "Piece.h"
#include <algorithm>
#include <tuple>
using std::pair;

//a composite of Rook and Bishop
bool Queen::legal_move_shape( pair< char , char > start , pair< char , char > end ) const {
  bool is_legal = false;

  //either the column or the row must be the same
  if (start.first == end.first) {
    is_legal = true;
  }
  
  if (start.second == end.second) {
    is_legal = true;
  }
  
  // take the differences in col/row
  int col_diff = start.first - end.first;
  int row_diff = start.second - end.second;
  
  //|slope| from one point to another must be 1
  //int slope = (int)row_diff / col_diff;
  if (abs(col_diff) == abs(row_diff)) {
    is_legal = true;
  }
  
  return is_legal;
}





