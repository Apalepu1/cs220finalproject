#ifndef CHESS_H
#define CHESS_H

#include <iostream>
#include <vector>
#include "Piece.h"
#include "Board.h"
#include <istream>
class Chess
{
public:
	// This default constructor initializes a board with the standard piece positions, and sets the state to white's turn
	Chess( void );
	Chess(std::istream& s);
	//~Chess() { delete[] &_board;}
	// Returns a constant reference to the board 
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	const Board& board( void ) const { return _board; }

	// Returns true if it's white's turn
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	bool turn_white( void ) const { return _turn_white; }

	// Change the turn
	void change_turn( void ) {_turn_white = !_turn_white;}
	
	// Attemps to make a move. If successfull, the move is made and the turn is switched white <-> black
	bool make_move( std::pair< char , char > start , std::pair< char , char > end );

	// Returns true if the designated player is in check
	bool in_check( bool white ) const;

	// Returns true if the designated player is in mate
	bool in_mate( bool white ) const;

	// Returns true if the designated player is in mate
	bool in_stalemate( bool white ) const;
	
	//make a vector of valid moves for a piece given 2 endpoints
	std::vector<std::pair<char, char>> make_pattern(const Piece* Piece, std::pair<char, char> start, std::pair<char, char> end) const;

	//check whether the path is clear
	bool path_is_clear(std::vector<std::pair<char, char>> pattern, const Board& board) const;

	bool valid_move( std::pair<char, char> start, std::pair<char, char> end) const;

	bool valid_capture(const Piece* piece, const Piece* end_piece) const;

	void loadNewBoard (std::istream& is);
	//if only the King piece is able to move, this is true
	bool onlyKingCanMove(bool white) const;
	//check if only the king can kill a threat, which will mean it can't really because that would result in another check.
        bool onlyKingCanKill(std::pair<char, char> position, bool white) const;
	//see if a space is blockable
	bool blockable(std::pair<char, char> position, bool white) const;
	//see if  this location has a threat
	bool has_threats(std::pair<char,char> position, bool white) const;
 private:
	// The board
	Board _board;

	// Is it white's turn?
	bool _turn_white;

};

// Writes the board out to a stream
std::ostream& operator << ( std::ostream& os , const Chess& chess );

// Reads the board in from a stream
std::istream& operator >> ( std::istream& is , Chess& chess );


#endif // CHESS_H
