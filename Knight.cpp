//Knight.cpp

#include "Knight.h"
#include "Piece.h"

#include <tuple>
using std::pair;

//legal example, b1 to b4
//illegal example, c1 to b3
bool Knight::legal_move_shape( pair< char , char > start , pair< char , char > end ) const {
  bool is_legal = false;

  // take the differences in col/row
  int col_diff = start.first - end.first;
  int row_diff = start.second - end.second;

  //|col_diff| and |row_diff| must be 1 or 2, exclusive
  switch (col_diff) {
  case 1: case -1:
    if (row_diff == 2 || row_diff == -2) {
      is_legal = true;
    }
    break;
  case 2: case -2:
    if (row_diff == 1 || row_diff == -1) {
      is_legal = true;
    }
    break;
  default:
    break;
  }

  return is_legal;  
}
