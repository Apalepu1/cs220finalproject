#include "Pawn.h"
#include "Piece.h"
#include <utility>

using std::pair;

bool Pawn::legal_move_shape( std::pair< char , char > start , std::pair< char , char > e\
nd ) const {
  
  //if the pawn is white, these are the legal moves
  if (this->is_white()) {
    if (start.second == '2') {
      if (start.first == end.first && ((end.second) - (start.second)) == 2) {
	return true;
      }
    }
    if (start.first == end.first && ((end.second) - (start.second) == 1)) {
      return true;
    }
  }
  
  //if the pawn is black, these are the legal moves
  if (!(this->is_white())) {
    if (start.second == '7') {
      if (start.first == end.first && ((start.second) - (end.second) == 2)) {
	return true;
      }
    }
    if (start.first == end.first && ((start.second) - (end.second) == 1)) {
      return true;
    }
  }
    
    return false;
}

bool Pawn::legal_capture_shape( std::pair< char , char > start , std::pair< char, char > end ) const {

  //legal capture shape for white pawn
  if (this->is_white()) {
    if (((end.first) - (start.first) == -1) || ((end.first) - (start.first) == 1)) {
      if (end.second == start.second + 1){
	return true;
      }
    }
  }
  //legal capture shape for black pawn
  if (!(this->is_white())) {
    if (((end.first) - (start.first) == -1) || ((end.first) - (start.first) == 1)){
      if (end.second == start.second - 1) {
	return true;
      }
    }
  }
  
  return false;
}

