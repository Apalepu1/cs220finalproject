#include "Chess.h"
#include<tuple>
#include<vector>
#include<algorithm>
#include<map>
using std::pair;
using std::vector;
using std::make_pair;
using std::map;


//for print statments to debug
#include<iostream>
using std::cout;
using std::endl;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess( void ) : _turn_white( true )
{
	// Add the pawns
	for( int i=0 ; i<8 ; i++ )
	{
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+1 ) , 'P' );
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+6 ) , 'p' );
	}

	// Add the rooks
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+7 ) , 'r' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+7 ) , 'n' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+7 ) , 'b' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+0 ) , 'Q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+0 ) , 'K' );
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+7 ) , 'q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+7 ) , 'k' );
}

void Chess::loadNewBoard(std::istream& s) {
  char mychar = '-';
  map<pair<char,char>, Piece* > copy = _board.get_board_copy();
  
  for(std::map< std::pair< char , char > , Piece* >::const_iterator it = copy.begin();
      it != copy.cend();
      ++it)
    {
      delete it->second;
  }
  _board.mapClear();
  for(int row = 7; row> -1; row--) {
    for(int column = 0; column < 8; column ++) {
      mychar = s.get();
      if(mychar != '-') {
	_board.add_piece( std::pair<char, char>( 'A' + column, '1' + row), mychar);
      }
      else {
      }
    }
    s.get();
  }
  if (s.get() == 'w') {
    _turn_white = true;
  } else {
    _turn_white = false;
  }
}
  
bool Chess::make_move( std::pair< char , char > start , std::pair< char , char > end ) {
  //check if it's your turn
  if (_board(start)== nullptr) {
    return false;
  }
  if (turn_white() == true && _board(start)->is_white() == false) {
    return false;
  }
  if (turn_white() == false && _board(start)->is_white() == true) {
      return false;
  }
  
  //check if its a legal move
  const Piece* piece = _board(start);
  if (!valid_move(start, end)) {
    return false;
  }

  //check if there's a piece already at end and perhaps capture it
  const Piece* end_piece = _board(end);
  if (end_piece != nullptr) {
  bool capture;
  capture = valid_capture(piece, end_piece); 
  if (capture == true) {
    _board.delete_at(end);
  }
  if (capture == false) {
    return false;
  }
  }
  
  //move the piece
  _board.add_piece(end, piece->to_ascii());
  _board.delete_at(start);
  //if the move leaves it in check, don't do it.
  if (in_check(turn_white())) {
    _board.delete_at(end);
    if (end_piece != nullptr) {
      _board.add_piece(end, end_piece->to_ascii());
    }
    _board.add_piece(start, piece->to_ascii());
    return false;
  }

  if (end.second == '1' || end.second =='8') {
    if (piece->to_ascii() == 'p') {
      _board.delete_at(end);
      _board.add_piece(end, 'q');
    } else if (piece->to_ascii() == 'P') {
      _board.delete_at(end);
      _board.add_piece(end, 'Q');
    }
  }
  change_turn();
  return true;
}

bool Chess::valid_move(pair<char, char> start, pair<char, char> end) const {
//check if start and end positions are the same
  if (start.first == end.first && start.second == end.second) {
    //cout << "start = end" << endl;
    return false;
  }
  if (_board(end) != nullptr && (_board(end)->is_white() == _board(start)->is_white())) {
    return false;
  }

  //check is positions valid board positions
  if (!(start.first >= 'A' && start.first <= 'H' && start.second >= '1' && start.second <= '8' && end.first >= 'A' && end.first <= 'H' && end.second >= '1' && end.second <= '8')){
    //cout << "outta bounds" << endl;
    return false;
  }

  //checking if piece exists at start
  if (!_board(start)) {
    //cout << "no piece at start" << endl;
    return false;
  }

  //check move legality
  const Piece* piece = _board(start);
  bool legal= true;
  if (!(*piece).legal_move_shape(start, end)) {
    //cout << "not a legal move shape" << endl;
    legal = false;
    if (piece->to_ascii() != 'p' && piece->to_ascii() != 'P') {
      return false;
    }
  }

  //check whether the path is clear if it applies to that piece (not knight)
  bool clear_path = true;

  if ((abs(start.first - end.first) == 1 || abs(start.second - end.second) == 1) && piece->to_ascii() != 'N' && piece->to_ascii() != 'n') {
    clear_path = true;
  } else {
   vector<pair<char, char>> pattern = make_pattern(piece, start, end);
   clear_path = path_is_clear(pattern, _board);
   if (clear_path == false) {
     //cout << "ya got blocked" << endl;
     return false;
   }
  }

  //legal capture shape for pawnie
  if (!legal) {
    if (piece->to_ascii() == 'p' || piece->to_ascii() == 'P') {
      if (_board(end) == nullptr) {
	return false;
      }
      else if (_board(end) != nullptr) {
	if (piece->legal_capture_shape(start, end) == false) {
	  return false;
	}
	else {
	  return true;
	}
      }
    }
  }

  return true;
}

bool Chess::valid_capture(const Piece* start_piece, const Piece* end_piece) const {

    //check whether there's a piece at the end position and maybe capture it
      if ((*end_piece).is_white() == (*start_piece).is_white()) {
	//cout << "piece of same color at end" << endl;
	return false;
      }
    return true;
}

//function to check write the squares over which a piece will be moving to test whether pa\ th is clear
vector<pair<char, char>> Chess::make_pattern(const Piece* Piece, std::pair<char, char> start, pair<char, char> end) const {

  char piece = (*Piece).to_ascii();
  vector<pair<char, char>> pattern;

  if (piece == 'q' || piece == 'Q' || piece == 'M' || piece == 'm') {
    if (start.first == end.first || start.second == end.second) {
      piece = 'r';
    } else if (abs(start.first-end.first) == abs(start.second-end.second)){
      piece = 'b';
      } else {
    }	
  }

  switch(piece) {
  case 'R': case 'r':
    if (start.first == end.first) { //move up or down
      if (start.second < end.second) { //moved up
        for (int i = ((int)(start.second + 1)); i < (int)end.second; i++) {
          char c = i;
          pattern.push_back(make_pair(start.first, c));
        }
        return pattern;
      }
      if (start.second > end.second) { //moved down
        for (char i = start.second - 1; i > end.second; i--) {
          pattern.push_back(make_pair(start.first, i));
        }
        return pattern;
      }
    }
    if (start.second == end.second) { //moving sideways

      if (start.first < end.first) { //moving right
        for (char i = start.first + 1; i < end.first; i++) {
          pattern.push_back(make_pair(i, start.second));
        }
        return pattern;
      }
      if (start.first > end.first) {//moving left
        for (char i = start.first - 1; i > end.first; i--) {
          pattern.push_back(make_pair(i, start.second));
        }
        return pattern;
      }
    }
    break;

  case 'P': case 'p':
    if (start.second < end.second) { // it's white and moving up
      pattern.push_back(make_pair(start.first, (start.second + 1)));
    }
    return pattern;
    if (start.second > end.second) { //it's black and moving down
      pattern.push_back(make_pair(start.first, (start.second - 1)));
    }
    return pattern;
    break;

  case 'B': case 'b':
    int length = abs(start.second - end.second) - 1;
    if (start.first < end.first && start.second < end.second) { //moving northeast
      for (int i = 0; i < length; i++) {
        pattern.push_back(make_pair(start.first + 1 + i, start.second + 1 + i));
      }
      return pattern;
    }
    if (start.first > end.first && start.second > end.second) { //moving southwest
      for (int i = 0; i < length; i++) {
        pattern.push_back(make_pair(start.first - 1 - i, start.second - 1 -i));
      }
      return pattern;
    }
    if (start.first > end.first && start.second < end.second) { //moving northwest
      for (int i = 0; i < length; i++) {
        pattern.push_back(make_pair(start.first - 1 - i, start.second + 1 + i));
      }
      return pattern;
    }
    if (start.first < end.first && start.second > end.second) { //moving southeast
      for (int i = 0; i < length; i++) {
        pattern.push_back(make_pair(start.first + 1 + i, start.second - 1 - i));
      }
      return pattern;
    }
    break;
  }

  return pattern;
}

//check whether the path is clear by checking each square in its path
bool Chess::path_is_clear(vector<pair<char, char>> pattern, const Board& board) const {
  const Piece* piece;

  for (vector<pair<char, char>>::const_iterator it = pattern.cbegin();
       it != pattern.cend();
       ++it) {
    piece = board(*it);
    if (piece != nullptr) {
      return false;
    }
  }
  return true;
}


//see if this location is blockable (diff method because can't consider pawn capture)
bool Chess::blockable(pair<char,char> position, bool white) const {
  //iterate through all the opponent's pieces
  map<pair<char,char>, Piece* > m = board().get_board_copy();
  for (map<pair<char, char>, Piece* >::const_iterator it = m.cbegin();
       it != m.cend();
       ++it)
    {

      //the player's turn is the opposite color to the opponent's pieces
      if (white != it->second->is_white()) {
        if (valid_move(it->first, position)) {
          cout<<"blocked by current: " << it->first.first << it->first.second<<endl;
        // cout << "position: " << _board(position)->to_ascii() << endl;
          return true;

        }
      }
    }

  return false;
}

//see if this location has a threat
bool Chess::has_threats(pair<char,char> position, bool white) const {
  //iterate through all the opponent's pieces
  map<pair<char,char>, Piece* > m = board().get_board_copy();
  for (map<pair<char, char>, Piece* >::const_iterator it = m.cbegin();
       it != m.cend();
       ++it)
    {
      
      //the player's turn is the opposite color to the opponent's pieces
      if (white != it->second->is_white()) {
	//cout << "current: " << it->first.first << it->first.second << " " << it->second->to_ascii() << endl;
	if (valid_move(it->first, position) && it->second->to_ascii() != 'p' && it->second->to_ascii() != 'P') {
	  // cout<<"current: " << it->first.first << it->first.second<<endl;
	// cout << "position: " << _board(position)->to_ascii() << endl;
	  return true;
	} else if (it->second->to_ascii() == 'p' || it->second->to_ascii() == 'P') {
		if (it->second->legal_capture_shape(it->first, position) == true) {
		  return true;
		}
		else {
		}
	      
	}
      }
    }

  return false;
}

bool Chess::onlyKingCanKill(pair<char, char> position, bool white) const {
  map<pair<char, char>, Piece* > m = board().get_board_copy();
    for (map<pair<char, char>, Piece* >::const_iterator it = m.cbegin();
       it != m.cend();
       ++it)
    {
      if (white == it->second->is_white()&&it->second->to_ascii() != 'K' && it->second->to_ascii() != 'k') {
	// cout << "current: " << it->first.first << it->first.second << " " << it->second->to_ascii() << endl;
        if (valid_move(it->first, position)) {

          return false;
        }
      }
    }
  return true;
}

bool Chess::onlyKingCanMove(bool white) const {
  map<pair<char, char>, Piece*> m = board().get_board_copy();
  for (map<pair<char, char>, Piece*>::const_iterator it = m.cbegin(); it != m.cend(); ++it) {
    if (it->second->is_white() == white && it->second->to_ascii() != 'K' && it->second->to_ascii()!= 'k') {
      for (int col = 'A'; col <= 'H'; col++) {
	for (int row = '1'; row <= '8'; row++) {
	  if (valid_move(it->first, make_pair(col, row))) {
	    return false;
	  }
	}
      }
    }
  }
  return true;
}
	    
bool Chess::in_check( bool white ) const
{
  //get a const copy of a map
  map<pair<char,char>, Piece* > m = board().get_board_copy();

  //find the player's king
  map<pair<char, char>, Piece* >::const_iterator king;
  for (map<pair<char, char>, Piece* >::const_iterator it = m.cbegin();
       it != m.cend();
       ++it)
    {
      if (it->second->to_ascii() == 'K' && white) {
	king = it;
      } else if (it->second->to_ascii() == 'k' && !white) {
	king = it;
      }
    }
  /*
  cout << "BEFORE trying to print king" << endl << endl;
  cout << "king: " << king->second->to_ascii() << endl << endl;
  cout << "AFTER trying to print king" << endl << endl;
  */  
  return has_threats(king->first, white);
}

bool Chess::in_mate( bool white ) const
{
  if (!in_check(white)) {
    return false;
  }
  if (in_check(!white)) {
    return false;
  }
  
  map<pair<char,char>, Piece* > m = _board.get_board_copy();
  
  /* after 1 move:
     1. block the path from the threat to the king
     2. capture the threat
     3. move the king out of the path
   */

  //find king
  map<pair<char, char>, Piece* >::const_iterator king;
  for (map<pair<char, char>, Piece* >::const_iterator it = m.cbegin();
       it != m.cend();
       ++it)
    {
      if (it->second->to_ascii() == 'K' && white) {
        king = it;
      } else if (it->second->to_ascii() == 'k' && !white) {
        king = it;
      }
    }
//check for all 8 places a king can move
  pair<char,char> king_loc = king->first;
  pair<char,char> go_north = make_pair( king_loc.first, king_loc.second + 1);
  pair<char,char> go_northeast = make_pair( king_loc.first + 1, king_loc.second + 1);
  pair<char,char> go_east = make_pair( king_loc.first + 1, king_loc.second);
  pair<char,char> go_southeast = make_pair( king_loc.first + 1, king_loc.second - 1);
  pair<char,char> go_south = make_pair( king_loc.first, king_loc.second - 1);
  pair<char,char> go_southwest = make_pair( king_loc.first - 1, king_loc.second - 1);
  pair<char,char> go_west = make_pair( king_loc.first - 1, king_loc.second);
  pair<char,char> go_northwest = make_pair( king_loc.first - 1, king_loc.second + 1);
  
  // there exists a move such that after it is made, in_mate will be false.
  bool move_possible = true;
  //cout<<"made it to king moving";
  if (valid_move(king_loc, go_north )) { //north
    if (!has_threats(go_north, !white)) {
      //cout<<"1";
      return false;
    }

  } else if (valid_move(king_loc, go_northeast )) { //northeast
    if (!has_threats(go_northeast, !white)) {
      //cout<<"2";
      return false;
    }
  } else if (valid_move(king_loc, go_east )) { //east
    if (!has_threats(go_east, !white)) {
      //cout<<"3";
      return false;
    }
  } else if (valid_move(king_loc, go_southeast )) { //southeast
    if (!has_threats(go_southeast,!white)) {
      //cout<<"4";
      return false;
    }
  } else if (valid_move(king_loc, go_south )) { //south
    if (!has_threats(go_south, !white)) {
      //cout<<"5";
      return false;
    }
  } else if (valid_move(king_loc, go_northwest )) { //southwest
    if (!has_threats(go_southwest, !white)) {
      //cout<<"6";
      return false;
    }
  } else if (valid_move(king_loc, go_west )) { //west
    if (!has_threats(go_west, !white)) {
      //cout<<"7";
      return false;
    }
  } else if (valid_move(king_loc, go_northwest )) { //northwest
    if (!has_threats(go_northwest, !white)) {
      //     cout<<"8";
      return false;
    }
  } else{
  
    //cout<<"King can't move"<<endl;
  //make vector of threats
  vector<pair<char, char>> threats;

  for (map<pair<char, char>, Piece* >::const_iterator it = m.cbegin();
       it != m.cend();
       ++it)
    {
      if (white != it->second->is_white()) {
        // a threat has the capability to capture the king
        if (valid_move(it->first, king->first)) {
          threats.push_back(it->first);
        }
      }
    }

  //if there is just one threat, it can be blocked or captured
  if (threats.size() == 1) {
    //cout<<"One threat only"<<endl;
    const Piece* p = _board(threats[0]);
    if (has_threats(threats[0], !white)) { //capture
      if (!onlyKingCanKill(threats[0], white)) {
	//	cout<<"can kill threat"<<endl;
	return false;
      }
    }

    //path between king and threat
    vector <pair<char, char> > pattern = make_pattern( p, threats[0], king_loc);

    for ( vector <pair<char, char> >::const_iterator it = pattern.cbegin();
	  it != pattern.cend();
	  ++it)
      {
	
	if ( blockable(*it, !white) ) {
	  return false;
	}
      }
  }
  }
  return move_possible;  
  
}

bool Chess::in_stalemate( bool white ) const
{
  if (in_check(white) || in_check(!white)) {
    //cout<< "someone in check tho" <<endl;
    return false;
  }
  if (!onlyKingCanMove(white)) {
    //cout<< "Something else can move!" <<endl;
    return false;
  }
  map<pair<char, char>, Piece* > m = board().get_board_copy();
  map<pair<char, char>, Piece* >::const_iterator king;
  for (map<pair<char, char>, Piece* >::const_iterator it = m.cbegin();
       it != m.cend();
       ++it)
    {
      if (it->second->to_ascii() == 'K' && white) {
        king = it;
      } else if (it->second->to_ascii() == 'k' && !white) {
        king = it;
      }
    }
  pair<char,char> king_loc = king->first;
  pair<char,char> go_north = make_pair( king_loc.first, king_loc.second + 1);
  pair<char,char> go_northeast = make_pair( king_loc.first + 1, king_loc.second + 1);
  pair<char,char> go_east = make_pair( king_loc.first + 1, king_loc.second);
  pair<char,char> go_southeast = make_pair( king_loc.first + 1, king_loc.second - 1);
  pair<char,char> go_south = make_pair( king_loc.first, king_loc.second - 1);
  pair<char,char> go_southwest = make_pair( king_loc.first - 1, king_loc.second - 1);
  pair<char,char> go_west = make_pair( king_loc.first - 1, king_loc.second);
  pair<char,char> go_northwest = make_pair( king_loc.first - 1, king_loc.second + 1);
  bool canMove = false;
  if (valid_move(king_loc, go_north )) { //north
    if (!has_threats(go_north, !white)) {
      //cout <<"northgood";
      canMove = true;
    }
  } else if (valid_move(king_loc, go_northeast )) { //northeast
    if (!has_threats(go_northeast, !white)) {
      canMove = true;
      //cout<<"northeastGood";
    }
  } else if (valid_move(king_loc, go_east )) { //east
    if (!has_threats(go_east, !white)) {
      canMove = true;
      //cout<<"eastgood";
    }
  } else if (valid_move(king_loc, go_southeast )) { //southeast
    if (!has_threats(go_southeast, !white)) {
      canMove = true;
      //cout<<"southeastgood";
    }
  } else if (valid_move(king_loc, go_south )) { //south
    if (!has_threats(go_south, !white)) {
      canMove = true;
      //cout<<"southgood";
    }
  } else if (valid_move(king_loc, go_northwest )) { //southwest
    if (!has_threats(go_southwest, !white)) {
      canMove = true;
      //cout<<"southwestgood";
    }
  } else if (valid_move(king_loc, go_west )) { //west
    if (!has_threats(go_west, !white)) {
      canMove = true;
      //cout<<"westGood";
    }
  } else if (valid_move(king_loc, go_northwest )) { //northwest
      if(!has_threats(go_west, !white)) {
	canMove = true;
	//cout<<"northwestGood";
      }
    }
  /////////////////////////
	// [REPLACE THIS STUB] //
	/////////////////////////
  return !canMove;
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Chess& chess )
{
	// Write the board out and then either the character 'w' or the character 'b', depending on whose turn it is
	return os << chess.board() << ( chess.turn_white() ? 'w' : 'b' );
}

std::istream& operator >> ( std::istream& is , Chess& chess )
{
  chess.loadNewBoard(is);
  return is;
}
